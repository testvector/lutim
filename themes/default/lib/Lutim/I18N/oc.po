# Lutim language file
# Copyright (C) 2014 Luc Didry
# This file is distributed under the same license as the Lutim package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Lutim\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Quentin Pagès\n"
"Language-Team: Occitan (http://www.transifex.com/fiat-tux/lutim/language/oc/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: oc\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. (7)
#. (30)
#. ($delay)
#. (config('max_delay')
#: lib/Lutim/Command/cron/stats.pm:149 lib/Lutim/Command/cron/stats.pm:150 lib/Lutim/Command/cron/stats.pm:160 lib/Lutim/Command/cron/stats.pm:161 lib/Lutim/Command/cron/stats.pm:177 lib/Lutim/Command/cron/stats.pm:178 themes/default/templates/partial/for_my_delay.html.ep:12 themes/default/templates/partial/for_my_delay.html.ep:13 themes/default/templates/partial/for_my_delay.html.ep:3 themes/default/templates/partial/lutim.js.ep:138 themes/default/templates/partial/lutim.js.ep:147 themes/default/templates/partial/lutim.js.ep:148 themes/default/templates/raw.html.ep:19 themes/default/templates/raw.html.ep:20 themes/default/templates/raw.html.ep:36 themes/default/templates/raw.html.ep:37 themes/default/templates/raw.html.ep:8 themes/default/templates/raw.html.ep:9
msgid "%1 days"
msgstr "%1 jorns"

#. ($total)
#: themes/default/templates/stats.html.ep:2
msgid "%1 sent images on this instance from beginning."
msgstr "%1 imatges mandats sus aquesta instància dempuèi lo començament."

#: themes/default/templates/index.html.ep:190
msgid "-or-"
msgstr "-o-"

#: lib/Lutim.pm:192 lib/Lutim/Command/cron/stats.pm:151 lib/Lutim/Command/cron/stats.pm:162 lib/Lutim/Command/cron/stats.pm:179 themes/default/templates/index.html.ep:5 themes/default/templates/raw.html.ep:10 themes/default/templates/raw.html.ep:21 themes/default/templates/raw.html.ep:38
msgid "1 year"
msgstr "1 an"

#: lib/Lutim.pm:191 lib/Lutim/Command/cron/stats.pm:148 lib/Lutim/Command/cron/stats.pm:159 lib/Lutim/Command/cron/stats.pm:176 themes/default/templates/index.html.ep:4 themes/default/templates/partial/for_my_delay.html.ep:12 themes/default/templates/partial/lutim.js.ep:147 themes/default/templates/raw.html.ep:18 themes/default/templates/raw.html.ep:35 themes/default/templates/raw.html.ep:7
msgid "24 hours"
msgstr "24 oras"

#: themes/default/templates/partial/myfiles.js.ep:57
msgid ": Error while trying to get the counter."
msgstr " : Error al moment de recuperar lo comptador."

#: lib/Lutim/Command/cron/stats.pm:144 themes/default/templates/raw.html.ep:3
msgid "Active images"
msgstr "Imatges actius"

#: lib/Lutim/Controller.pm:288
msgid "An error occured while downloading the image."
msgstr "Una error es apareguda pendent lo telecargament de l'imatge."

#: themes/default/templates/zip.html.ep:2
msgid "Archives download"
msgstr "Telecargar los archius"

#: themes/default/templates/about.html.ep:41 themes/default/templates/myfiles.html.ep:64 themes/default/templates/stats.html.ep:25
msgid "Back to homepage"
msgstr "Tornar a la pagina d'acuèlh"

#: themes/default/templates/index.html.ep:193 themes/default/templates/index.html.ep:194
msgid "Click to open the file browser"
msgstr "Clicatz per utilizar lo navigator de fichièr"

#: themes/default/templates/about.html.ep:30
msgid "Contributors"
msgstr "Contribudors"

#: themes/default/templates/partial/lutim.js.ep:214 themes/default/templates/partial/lutim.js.ep:268 themes/default/templates/partial/lutim.js.ep:346
msgid "Copy all view links to clipboard"
msgstr "Copiar totes los ligams de visualizacion dins lo quichapapièrs"

#: themes/default/templates/index.html.ep:18 themes/default/templates/index.html.ep:36 themes/default/templates/index.html.ep:69 themes/default/templates/index.html.ep:77 themes/default/templates/index.html.ep:85 themes/default/templates/index.html.ep:93 themes/default/templates/myfiles.html.ep:20 themes/default/templates/myfiles.html.ep:38 themes/default/templates/partial/common.js.ep:150 themes/default/templates/partial/lutim.js.ep:105 themes/default/templates/partial/lutim.js.ep:120 themes/default/templates/partial/lutim.js.ep:79 themes/default/templates/partial/lutim.js.ep:91
msgid "Copy to clipboard"
msgstr "Copiar dins lo quichapapièrs"

#: themes/default/templates/myfiles.html.ep:52
msgid "Counter"
msgstr "Comptador"

#: themes/default/templates/stats.html.ep:18
msgid "Delay repartition chart for disabled images"
msgstr "Grafic de despartiment dels delais pels imatges desactivats"

#: themes/default/templates/stats.html.ep:15
msgid "Delay repartition chart for enabled images"
msgstr "Grafic de despartiment dels delais pels imatges activats"

#: themes/default/templates/index.html.ep:115 themes/default/templates/index.html.ep:147 themes/default/templates/index.html.ep:178 themes/default/templates/myfiles.html.ep:53 themes/default/templates/partial/lutim.js.ep:159
msgid "Delete at first view?"
msgstr "Suprimir al primièr accès ?"

#: lib/Lutim/Command/cron/stats.pm:145 themes/default/templates/raw.html.ep:4
msgid "Deleted images"
msgstr "Imatges suprimits"

#: lib/Lutim/Command/cron/stats.pm:146 themes/default/templates/raw.html.ep:5
msgid "Deleted images in 30 days"
msgstr "Imatges per èsser suprimits dins 30 jorns"

#: themes/default/templates/index.html.ep:98 themes/default/templates/myfiles.html.ep:56 themes/default/templates/partial/common.js.ep:142 themes/default/templates/partial/common.js.ep:145
msgid "Deletion link"
msgstr "Ligam de supression"

#: themes/default/templates/gallery.html.ep:6
msgid "Download all images"
msgstr "Telecargar totes los imatges"

#: themes/default/templates/index.html.ep:81 themes/default/templates/index.html.ep:83 themes/default/templates/partial/lutim.js.ep:101 themes/default/templates/partial/lutim.js.ep:97
msgid "Download link"
msgstr "Ligam de telecargament"

#: themes/default/templates/index.html.ep:28 themes/default/templates/index.html.ep:31 themes/default/templates/myfiles.html.ep:30 themes/default/templates/myfiles.html.ep:33
msgid "Download zip link"
msgstr "Ligam de telecargament de l'archiu dels imatges"

#: themes/default/templates/index.html.ep:189
msgid "Drag & drop images here"
msgstr "Lisatz e depausatz vòstres imatges aquí"

#: themes/default/templates/about.html.ep:7
msgid "Drag and drop an image in the appropriate area or use the traditional way to send files and Lutim will provide you four URLs. One to view the image, an other to directly download it, one you can use on social networks and a last to delete the image when you want."
msgstr "Depausatz vòstres imatges dins la zòna prevista per aquò o seleccionatz un fichièr d'un biais classic e Lutim vos donarà quatre URLs en torna. Una per afichar l'imatge, una mai per lo telecargar dirèctament, una per l'utilizar suls malhums socials e una darrièra per suprimir vòstre imatge quand volguèssetz."

#: themes/default/templates/index.html.ep:150 themes/default/templates/index.html.ep:181
msgid "Encrypt the image (Lutim does not keep the key)."
msgstr "Chifrar l'imatge (Lutim garda pas la clau)."

#: themes/default/templates/partial/lutim.js.ep:44
msgid "Error while trying to modify the image."
msgstr "Una error es apareguda al moment de modificar l'imatge."

#: themes/default/templates/stats.html.ep:10
msgid "Evolution of total files"
msgstr "Evolucion del nombre total de fichièrs"

#: themes/default/templates/myfiles.html.ep:55
msgid "Expires at"
msgstr "S'acaba lo"

#: themes/default/templates/myfiles.html.ep:50
msgid "File name"
msgstr "Nom del fichièr"

#: themes/default/templates/about.html.ep:24
msgid "For more details, see the <a href=\"https://framagit.org/luc/lutim\">homepage of the project</a>."
msgstr "Per mai de detalhs, consultatz la pagina <a href=\"https://framagit.org/luc/lutim\">Github</a> del projècte."

#: themes/default/templates/layouts/default.html.ep:55
msgid "Fork me!"
msgstr "Tustatz-me !"

#: themes/default/templates/index.html.ep:10 themes/default/templates/index.html.ep:13 themes/default/templates/myfiles.html.ep:12 themes/default/templates/myfiles.html.ep:15
msgid "Gallery link"
msgstr "Ligam  cap a la galariá"

#: themes/default/templates/partial/common.js.ep:104 themes/default/templates/partial/common.js.ep:87
msgid "Hit Ctrl+C, then Enter to copy the short link"
msgstr "Fasètz Ctrl+C puèi picatz Entrada per copiar lo ligam"

#: themes/default/templates/layouts/default.html.ep:50
msgid "Homepage"
msgstr "Acuèlh"

#: themes/default/templates/about.html.ep:20
msgid "How do you pronounce Lutim?"
msgstr "Cossí cal prononciar Lutim ?"

#: themes/default/templates/about.html.ep:6
msgid "How does it work?"
msgstr "Cossí aquò marcha ?"

#: themes/default/templates/about.html.ep:18
msgid "How to report an image?"
msgstr "Qué far per senhalar un imatge ?"

#: themes/default/templates/about.html.ep:14
msgid "If the files are deleted if you ask it while posting it, their SHA512 footprint are retained."
msgstr "Se los fichièrs son ben estats suprimits se o avètz demandat, lors signaturas SHA512 son gardadas."

#: themes/default/templates/index.html.ep:163 themes/default/templates/index.html.ep:203
msgid "Image URL"
msgstr "URL de l'imatge"

#: lib/Lutim/Command/cron/stats.pm:143 themes/default/templates/raw.html.ep:2
msgid "Image delay"
msgstr "Delai de l'imatge"

#: lib/Lutim/Controller.pm:710
msgid "Image not found."
msgstr "Imatge pas trobat."

#: themes/default/templates/layouts/default.html.ep:54
msgid "Informations"
msgstr "Informacions"

#: themes/default/templates/layouts/default.html.ep:62
msgid "Install webapp"
msgstr "Installar la webapp"

#: themes/default/templates/layouts/default.html.ep:61
msgid "Instance's statistics"
msgstr "Estatisticas de l'instància"

#: themes/default/templates/about.html.ep:11
msgid "Is it really anonymous?"
msgstr "Es vertadièrament anonim ?"

#: themes/default/templates/about.html.ep:9
msgid "Is it really free (as in free beer)?"
msgstr "Es vertadièrament gratuit ?"

#: themes/default/templates/about.html.ep:21
msgid "Juste like you pronounce the French word <a href=\"https://fr.wikipedia.org/wiki/Lutin\">lutin</a> (/ly.tɛ̃/)."
msgstr "Òm pronóncia coma en occitan lengadocian, LU-TI-N, amb una M finala que sona N, o coma la paraula francesa lutin (/ly.tɛ̃/)."

#: themes/default/templates/index.html.ep:153 themes/default/templates/index.html.ep:184
msgid "Keep EXIF tags"
msgstr "Conservar las donadas EXIF"

#: themes/default/templates/index.html.ep:118 themes/default/templates/index.html.ep:166 themes/default/templates/index.html.ep:206 themes/default/templates/partial/lutim.js.ep:163
msgid "Let's go!"
msgstr "Zo !"

#: themes/default/templates/layouts/default.html.ep:58
msgid "Liberapay button"
msgstr "Boton Liberapay"

#: themes/default/templates/layouts/default.html.ep:53
msgid "License:"
msgstr "Licéncia :"

#: themes/default/templates/index.html.ep:89 themes/default/templates/index.html.ep:91 themes/default/templates/partial/lutim.js.ep:111 themes/default/templates/partial/lutim.js.ep:115
msgid "Link for share on social networks"
msgstr "Ligam per partejar suls malhums socials"

#: themes/default/templates/zip.html.ep:7
msgid "Lutim can't zip so many images at once, so it splitted your demand in multiple URLs."
msgstr "Lutim pòt pas comprimir tan d'imatge d'un còp, a doncas trocejat vòstra demanda en multiplas URLs"

#: themes/default/templates/about.html.ep:4
msgid "Lutim is a free (as in free beer) and anonymous image hosting service. It's also the name of the free (as in free speech) software which provides this service."
msgstr "Lutim es un servici gratuit e anonim d’albergament d’imatges. S’agís tanben del nom del logicial (liure) que fornís aqueste servici."

#: themes/default/templates/about.html.ep:25
msgid "Main developers"
msgstr "Desvolopaires de l'aplicacion"

#: themes/default/templates/index.html.ep:73 themes/default/templates/index.html.ep:75 themes/default/templates/partial/lutim.js.ep:85 themes/default/templates/partial/lutim.js.ep:88
msgid "Markdown syntax"
msgstr "Sintaxi Markdown"

#: themes/default/templates/layouts/default.html.ep:60 themes/default/templates/myfiles.html.ep:2
msgid "My images"
msgstr "Mos imatges"

#: themes/default/templates/partial/myfiles.js.ep:19
msgid "No limit"
msgstr "Pas cap de data d'expiracion"

#: themes/default/templates/index.html.ep:165 themes/default/templates/index.html.ep:198
msgid "Only images are allowed"
msgstr "Solament son acceptats los imatges"

#: themes/default/templates/myfiles.html.ep:6
msgid "Only the images sent with this browser will be listed here. The informations are stored in localStorage: if you delete your localStorage data, you'll loose this informations."
msgstr "Solament los imatges mandats amb aqueste navigador seràn listats aquí. Las informacions son gardadas en localStorage : se escafatz vòstras donadas localStorage, perdretz aquelas informacions."

#: themes/default/templates/about.html.ep:16
msgid "Only the uploader! (well, only if he's the only owner of the images' rights before the upload)"
msgstr "Solament lo qu'a mandat ! (ben, solament se ten los dreits exclusius dels imatges abans de los mandar)"

#: themes/default/templates/zip.html.ep:12
msgid "Please click on each URL to download the different zip files."
msgstr "Mercés de clicar sus cada URL per telecargar los diferents archius ZIP."

#. (config('contact')
#: themes/default/templates/about.html.ep:19
msgid "Please contact the administrator: %1"
msgstr "Mercés de contactar l'administrator : %1"

#: themes/default/templates/stats.html.ep:22
msgid "Raw stats"
msgstr "Estatisticas brutas"

#: themes/default/templates/index.html.ep:158
msgid "Send an image"
msgstr "Mandar un imatge"

#: themes/default/templates/partial/lutim.js.ep:20
msgid "Share it!"
msgstr "Partejatz-lo !"

#: themes/default/templates/layouts/default.html.ep:56
msgid "Share on Twitter"
msgstr "Partejar sus Twitter"

#: themes/default/templates/index.html.ep:133 themes/default/templates/partial/lutim.js.ep:174
msgid "Something bad happened"
msgstr "Un problèma es aparegut"

#. ($c->config('contact')
#: lib/Lutim/Controller.pm:717
msgid "Something went wrong when creating the zip file. Try again later or contact the administrator (%1)."
msgstr "Quicòm a trucat pendent la creacion de l'archiu. Mercés de tornar ensajar pus tard o de contactar l'administrator (%1)."

#: themes/default/templates/layouts/default.html.ep:58
msgid "Support the author on Liberapay"
msgstr "Sostenir l'autor sus Liberapay"

#: themes/default/templates/layouts/default.html.ep:57
msgid "Support the author on Tipeee"
msgstr "Sostenir l'autor sus Tipeee"

#: themes/default/templates/about.html.ep:13
msgid "The IP address of the image's sender is retained for a delay which depends of the administrator's choice (for the official instance, which is located in France, it's one year)."
msgstr "L’IP de la persona que mandèt l'imatge es gardada pendent un delai que depend de l'administrator de l'instància (per l'instància oficiala que lo servidor es en França, es un delai d'un an)."

#: themes/default/templates/about.html.ep:23
msgid "The Lutim software is a <a href=\"http://en.wikipedia.org/wiki/Free_software\">free software</a>, which allows you to download and install it on you own server. Have a look at the <a href=\"https://www.gnu.org/licenses/agpl-3.0.html\">AGPL</a> to see what you can do."
msgstr "Lo logicial Lutim es un <a href=\"https://oc.wikipedia.org/wiki/Logicial_liure\">logicial liure</a>, que permet de lo telecargar e de l’installar sus vòstre pròpri servidor. Gaitatz l’<a href=\"https://www.gnu.org/licenses/agpl-3.0.html\">AGPL</a> per veire que son vòstres dreits"

#: lib/Lutim/Controller.pm:307
msgid "The URL is not valid."
msgstr "L'URL n'es pas valida."

#: themes/default/templates/zip.html.ep:16
msgid "The automatic download process will open a tab in your browser for each link. You need to allow popups for Lutim."
msgstr "Lo processús automatic de telecargament obrirà un onglet dins lo navigato per cada ligam. Vos cal autorizar las popups per Lutim."

#: lib/Lutim/Controller.pm:120 lib/Lutim/Controller.pm:188
msgid "The delete token is invalid."
msgstr "Lo geton de supression es invalida."

#. ($upload->filename)
#: lib/Lutim/Controller.pm:445
msgid "The file %1 is not an image."
msgstr "Lo fichièr %1 es pas un imatge."

#. ($tx->res->max_message_size)
#. ($c->req->max_message_size)
#. (config('max_file_size')
#: lib/Lutim/Controller.pm:271 lib/Lutim/Controller.pm:340 themes/default/templates/partial/lutim.js.ep:240
msgid "The file exceed the size limit (%1)"
msgstr "Lo fichièr depassa lo limit de talha (%1)"

#: themes/default/templates/stats.html.ep:12
msgid "The graph's datas are not updated in real-time."
msgstr "Las donadas del grafic son pas mesas a jorn en temps real."

#. ($image->filename)
#: lib/Lutim/Controller.pm:190
msgid "The image %1 has already been deleted."
msgstr "L'imatge %1 es ja estat suprimit."

#. ($image->filename)
#: lib/Lutim/Controller.pm:199 lib/Lutim/Controller.pm:204
msgid "The image %1 has been successfully deleted"
msgstr "L'imatge %1 es estat suprimit amb succès."

#: lib/Lutim/Controller.pm:128
msgid "The image's delay has been successfully modified"
msgstr "Lo delai de l'imatge es plan estat modificat."

#: themes/default/templates/index.html.ep:45
msgid "The images are encrypted on the server (Lutim does not keep the key)."
msgstr "Los imatges son chifrats sul servidor (Lutim garda pas la clau)."

#: themes/default/templates/about.html.ep:5
msgid "The images you post on Lutim can be stored indefinitely or be deleted at first view or after a delay selected from those proposed."
msgstr "Los imatges depausats sus Lutim pòdon èsser gardats sens fin, o s’escafar tre lo primièr afichatge o al cap d'un delai causit entre los prepausats."

#. ($c->config->{contact})
#: lib/Lutim/Controller.pm:442
msgid "There is no more available URL. Retry or contact the administrator. %1"
msgstr "I a pas mai d'URL disponibla. Mercés de tornar ensajar o de contactar l'administrator. %1."

#: themes/default/templates/layouts/default.html.ep:57
msgid "Tipeee button"
msgstr "Boton Tipeee"

#: lib/Lutim/Command/cron/stats.pm:152 themes/default/templates/raw.html.ep:11
msgid "Total"
msgstr "Total"

#: themes/default/templates/index.html.ep:60 themes/default/templates/partial/lutim.js.ep:14
msgid "Tweet it!"
msgstr "Tweetejatz-lo !"

#. ($short)
#: lib/Lutim/Controller.pm:162 lib/Lutim/Controller.pm:233
msgid "Unable to find the image %1."
msgstr "Impossible de trobar l'imatge %1."

#: lib/Lutim/Controller.pm:529 lib/Lutim/Controller.pm:574 lib/Lutim/Controller.pm:616 lib/Lutim/Controller.pm:659 lib/Lutim/Controller.pm:671 lib/Lutim/Controller.pm:682 lib/Lutim/Controller.pm:707 lib/Lutim/Plugin/Helpers.pm:57
msgid "Unable to find the image: it has been deleted."
msgstr "Impossible de trobar l'imatge : es estat suprimit."

#: lib/Lutim/Controller.pm:105
msgid "Unable to get counter"
msgstr "Impossible de recuperar lo comptador"

#: themes/default/templates/about.html.ep:17
msgid "Unlike many image sharing services, you don't give us rights on uploaded images."
msgstr "A l'invèrse de la màger part dels servicis de partiment d'imatge, daissatz pas cap de dreit suls imatges que mandatz."

#: themes/default/templates/index.html.ep:162 themes/default/templates/index.html.ep:201
msgid "Upload an image with its URL"
msgstr "Depausar un imatge per son URL"

#: themes/default/templates/myfiles.html.ep:54
msgid "Uploaded at"
msgstr "Mandat lo"

#: themes/default/templates/stats.html.ep:6
msgid "Uploaded files by days"
msgstr "Fichièrs mandats per jorn"

#. ($c->app->config('contact')
#: lib/Lutim/Plugin/Helpers.pm:152
msgid "Uploading is currently disabled, please try later or contact the administrator (%1)."
msgstr "La mesa en linha es desactivada pel moment, mercés de tornar ensajar mai tard o de contactar l'administrator (%1)."

#: themes/default/templates/index.html.ep:65 themes/default/templates/index.html.ep:67 themes/default/templates/myfiles.html.ep:51 themes/default/templates/partial/lutim.js.ep:71 themes/default/templates/partial/lutim.js.ep:75
msgid "View link"
msgstr "Ligam d'afichatge"

#: themes/default/templates/about.html.ep:22
msgid "What about the software which provides the service?"
msgstr "E a prepaus del logicial que fornís lo servici ?"

#: themes/default/templates/about.html.ep:3
msgid "What is Lutim?"
msgstr "Qu’es aquò Lutim ?"

#: themes/default/templates/about.html.ep:15
msgid "Who owns rights on images uploaded on Lutim?"
msgstr "Qual a los dreits suls imatges mandats sus Lutim ?"

#: themes/default/templates/about.html.ep:12
msgid "Yes, it is! On the other side, for legal reasons, your IP address will be stored when you send an image. Don't panic, it is normally the case of all sites on which you send files!"
msgstr "Òc, es aquò ! Al contrari, per de rasons legalas, vòstra adreça IP serà enregistrada quand mandaretz un imatge. Perdetz pas lo cap, normalament es çò normal pels demai dels sites suls quals mandatz de fichièrs !"

#: themes/default/templates/about.html.ep:10
msgid "Yes, it is! On the other side, if you want to support the developer, you can do it via <a href=\"https://www.tipeee.com/fiat-tux\">Tipeee</a> or via <a href=\"https://liberapay.com/sky/\">Liberapay</a>."
msgstr "Òc, o es ! Al contrari, s'avètz enveja de sosténer lo desvolopaire, podètz far un microdon amb <a href=\"https://www.tipeee.com/fiat-tux\">Tipeee</a> o via <a href=\"https://liberapay.com/sky/\">Liberapay</a>."

#: themes/default/templates/zip.html.ep:6
msgid "You asked to download a zip archive for too much files."
msgstr "Avètz demandat de telecargar tròp d'imatges d'un còp dins un archiu ZIP."

#: themes/default/templates/about.html.ep:8
msgid "You can, optionally, request that the image(s) posted on Lutim to be deleted at first view (or download) or after the delay selected from those proposed."
msgstr "Podètz, d'un biais facultatiu, demandar que l'imatge o los imatges depausats sus Lutim sián suprimits aprèp lor primièr afichatge (o telecargament) o al cap d'un delai causit entre las prepausadas."

#: themes/default/templates/about.html.ep:27
msgid "and on"
msgstr "e sus"

#: themes/default/templates/about.html.ep:27
msgid "core developer"
msgstr "desvolopaire màger"

#: lib/Lutim.pm:190 lib/Lutim/Command/cron/stats.pm:147 lib/Lutim/Command/cron/stats.pm:158 lib/Lutim/Command/cron/stats.pm:175 themes/default/templates/index.html.ep:3 themes/default/templates/raw.html.ep:17 themes/default/templates/raw.html.ep:34 themes/default/templates/raw.html.ep:6
msgid "no time limit"
msgstr "Pas cap de limitacion de durada"

#: themes/default/templates/about.html.ep:38
msgid "occitan translation"
msgstr "traduccion en occitan"

#: themes/default/templates/about.html.ep:27
msgid "on"
msgstr "sus"

#: themes/default/templates/about.html.ep:36
msgid "spanish translation"
msgstr "traduccion en espanhòl"

#: themes/default/templates/about.html.ep:28
msgid "webapp developer"
msgstr "desvolopaire de la webapp"
